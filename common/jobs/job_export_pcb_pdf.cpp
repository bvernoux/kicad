/*
 * This program source code file is part of KiCad, a free EDA CAD application.
 *
 * Copyright (C) 2023 Mark Roszko <mark.roszko@gmail.com>
 * Copyright (C) 2023 KiCad Developers, see AUTHORS.txt for contributors.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <jobs/job_export_pcb_pdf.h>
#include <jobs/job_registry.h>
#include <i18n_utility.h>

NLOHMANN_JSON_SERIALIZE_ENUM( JOB_EXPORT_PCB_PDF::DRILL_MARKS,
                              {
                                { JOB_EXPORT_PCB_PDF::DRILL_MARKS::NO_DRILL_SHAPE, "none" },
                                { JOB_EXPORT_PCB_PDF::DRILL_MARKS::SMALL_DRILL_SHAPE, "small" },
                                { JOB_EXPORT_PCB_PDF::DRILL_MARKS::FULL_DRILL_SHAPE, "full" }
                              } )

JOB_EXPORT_PCB_PDF::JOB_EXPORT_PCB_PDF() :
        JOB_EXPORT_PCB_PLOT( JOB_EXPORT_PCB_PLOT::PLOT_FORMAT::PDF, "pdf", false )
{
    m_params.emplace_back( new JOB_PARAM<wxString>( "color_theme", &m_colorTheme, m_colorTheme ) );
    m_params.emplace_back( new JOB_PARAM<bool>( "mirror", &m_mirror, m_mirror ) );
    m_params.emplace_back(
            new JOB_PARAM<bool>( "black_and_white", &m_blackAndWhite, m_blackAndWhite ) );
    m_params.emplace_back( new JOB_PARAM<bool>( "negative", &m_negative, m_negative ) );
    m_params.emplace_back( new JOB_PARAM<bool>( "plot_footprint_values", &m_plotFootprintValues,
                                                m_plotFootprintValues ) );
    m_params.emplace_back( new JOB_PARAM<bool>( "plot_pad_numbers", &m_plotPadNumbers, m_plotPadNumbers ) );
    m_params.emplace_back( new JOB_PARAM<bool>( "plot_invisible_text", &m_plotInvisibleText,
                                                m_plotInvisibleText ) );
    m_params.emplace_back( new JOB_PARAM<bool>(
            "sketch_pads_on_fab_layers", &m_sketchPadsOnFabLayers, m_sketchPadsOnFabLayers ) );
    m_params.emplace_back(
            new JOB_PARAM<DRILL_MARKS>( "drill_shape", &m_drillShapeOption, m_drillShapeOption ) );
}


wxString JOB_EXPORT_PCB_PDF::GetDefaultDescription() const
{
    return wxString::Format( _( "PCB PDF export" ) );
}


REGISTER_JOB( pcb_export_pdf, _HKI( "PCB: Export PDF" ), KIWAY::FACE_PCB, JOB_EXPORT_PCB_PDF );